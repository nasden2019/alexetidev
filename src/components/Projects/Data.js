import React from 'react';

export const data = [
    {
        id: 0,
        links: 'https://sendasverdes.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fsendasverdes.netlify.app'
    },
    {
        id: 1,
        links: 'https://www.alexetidev.com/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fwww.alexetidev.com',
    },
    {
        id: 2,
        links: 'https://shopshi.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fshopshi.netlify.app',
    },
    {
        id: 3,
        links: 'https://cubus.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fcubus.netlify.app%2Fseguros.html',
    },
    {
        id: 4,
        links: 'https://ziolukagency.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fziolukagency.netlify.app',
    },
    {
        id: 5,
        links: 'https://creaciones.netlify.app',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Finfallible-bohr-6ff2af.netlify.app%2Fconvocatorias.html',
    },
    {
        id: 6,
        links: 'https://vantha-realstate.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fvantha-realstate.netlify.app',
    },
    {
        id: 7,
        links: 'https://zealous-hamilton-180c96.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fmuds.netlify.app',
    },
    {
        id: 8,
        links: 'https://jolly-galileo-a7fc7c.netlify.app/',
        thumbnail: 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fjolly-galileo-a7fc7c.netlify.app',
    },
]

export default data