import React from 'react';
import data from './Data'

const ProjectList = () => {

const [projects, setProjects] = React.useState(data);

// display text on hover:
const [isShown, setIsShown] = React.useState(false);
const [isShown2, setIsShown2] = React.useState(false);
const [isShown3, setIsShown3] = React.useState(false);
const [isShown4, setIsShown4] = React.useState(false);
const [isShown5, setIsShown5] = React.useState(false);
const [isShown6, setIsShown6] = React.useState(false);
const [isShown7, setIsShown7] = React.useState(false);
const [isShown8, setIsShown8] = React.useState(false);
const [isShown9, setIsShown9] = React.useState(false);

// function changeBackground(e) {
//   e.target.style.background = 'red';
// }

const { id, links, thumbnail } = projects;

    return (
        <div className="container-fluid" id="projects" >
             <div className="row">
                 <div className="col-sm-12">
                   <h2 className="h2titleP text-center">Projects</h2>
                   <div className="titleUnderlineP"></div>
                     <div className="row">
                         {/* {
                           projects.map(project => (
                             <div key={project.id} className="col-sm-4 mb-5">
                               <a href={project.links}>
                                   <img src={project.thumbnail} alt="" className="img-fluid" />
                               </a>
                            </div>
                           ))   
                         } */}
                            <div className="col-sm-4 mb-5">
                               <a href="https://shopshi.netlify.app/" 
                                      onMouseEnter={() => setIsShown(true)}
                                      onMouseLeave={() => setIsShown(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fshopshi.netlify.app" alt="" className="img-fluid" />
                               {isShown && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Original Merchandising: Clothes, Calendars
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://sendasverdes.netlify.app/" 
                                      onMouseEnter={() => setIsShown2(true)}
                                      onMouseLeave={() => setIsShown2(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fsendasverdes.netlify.app" alt="" className="img-fluid" />
                               {isShown2 && (
                                 <div className="projectEffect text-center">
                                   <p className="projectText ">
                                     Cactus and Plants Shop
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://www.alexetidev.com/" 
                                      onMouseEnter={() => setIsShown3(true)}
                                      onMouseLeave={() => setIsShown3(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fwww.alexetidev.com" alt="" className="img-fluid" />
                               {isShown3 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Actor and Model Portfolio
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                              <div className="col-sm-4 mb-5">
                               <a href="https://ziolukagency.netlify.app/" 
                                      onMouseEnter={() => setIsShown4(true)}
                                      onMouseLeave={() => setIsShown4(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fziolukagency.netlify.app" alt="" className="img-fluid" />
                               {isShown4 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Content Marketing and Strategy Agency
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div> 
                            <div className="col-sm-4 mb-5">
                               <a href="https://creaciones.netlify.app/" 
                                      onMouseEnter={() => setIsShown5(true)}
                                      onMouseLeave={() => setIsShown5(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fcreaciones.netlify.app" alt="" className="img-fluid" />
                               {isShown5 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Agriculture Ecosystem and Machinery
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://vantha-realstate.netlify.app/" 
                                      onMouseEnter={() => setIsShown6(true)}
                                      onMouseLeave={() => setIsShown6(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fvantha-realstate.netlify.app" alt="" className="img-fluid" />
                               {isShown6 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Real State: Houses, Departments, Resorts
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://cubus.netlify.app/" 
                                      onMouseEnter={() => setIsShown7(true)}
                                      onMouseLeave={() => setIsShown7(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fcubus.netlify.app%2Fseguros.html" alt="" className="img-fluid" />
                               {isShown7 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Insurance Consulting Services
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://muds.netlify.app/" 
                                      onMouseEnter={() => setIsShown8(true)}
                                      onMouseLeave={() => setIsShown8(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fmuds.netlify.app" alt="" className="img-fluid" />
                               {isShown8 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Co-workers and Team App
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>
                            <div className="col-sm-4 mb-5">
                               <a href="https://jolly-galileo-a7fc7c.netlify.app/" 
                                      onMouseEnter={() => setIsShown9(true)}
                                      onMouseLeave={() => setIsShown9(false)}>
                                   <img src="http://free.pagepeeker.com/v2/thumbs.php?size=x&url=https%3A%2F%2Fjolly-galileo-a7fc7c.netlify.app" alt="" className="img-fluid" />
                               {isShown9 && (
                                 <div className="projectEffect">
                                   <p className="projectText">
                                     Software Development Services
                                   </p>
                                 </div>
                               )}
                               </a>
                            </div>  
                     </div>         
                 </div>
             </div>
        </div>
    )
}

export default ProjectList
