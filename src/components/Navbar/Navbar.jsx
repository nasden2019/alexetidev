import React, { useState, useRef, useEffect } from 'react'
import { FaBars, FaTwitter, FaInstagram } from 'react-icons/fa'
import { links, social } from './DataNav';
import { Link } from 'react-scroll';
// import { Link } from 'react-router-dom';

// import ProjectList from '../Projects/ProjectList'

// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Link,
//     NavLink
//   } from "react-router-dom";

// import ProjectList from '../Projects/ProjectList';
// import Home from '../../App';
// import OwlC from '../Slider/OwlC';
// import Animation from '../Aboutme/Animation'


const Navbar = () => {

const [showLinks, setShowLinks] = useState(false);
// const [hideNav, setHideNav] = useState(false);
const [navbar, setNavbar] = useState(false);


// animation of navbar on scroll
const changeBackground = () => {
    console.log(window.scrollY)
    if (window.scrollY >= 100) {
        setNavbar(true)
    } else {
        setNavbar(false)
    }
}
window.addEventListener('scroll', changeBackground);


// const hideNav = () => {
//   document.querySelector('.show-container').display = 'hide';
// }

// Scrobable Links
const scrollToSectionH = () => {
  document.querySelector('#home').scrollIntoView({
    behavior: 'smooth'
  });
}
const scrollToSectionAbout = () => {
  document.querySelector('#aboutme').scrollIntoView({
    paddingTop: '60px',
    behavior: 'smooth',
  });
}
const scrollToSectionP = () => {
  document.querySelector('#projects').scrollIntoView({
    behavior: 'smooth',
    margin: '50px',
  });
}
const scrollToSectionS = () => {
  document.querySelector('#myskills').scrollIntoView({
    behavior: 'smooth'
  });
}

    return (
        <>
        <nav className={ navbar ? 'activeNav fixed-top' : ''}>
            <div className="navbar-center">
                <div className="navbar-header flex-column">
                    {/* <h3 className="logo">Alex Declercq</h3>
                      <h4 className="logo">Frontend Developer</h4>      */}
                    <button  onClick={() => setShowLinks(!showLinks)}
                             className="navbar-toggle">
                        <FaBars />
                    </button>
                 </div>   

               {/* button will display links because of conditional rendering: */}
                 {/* {
                     showLinks &&  */}

                     {/* Used this way working fine (react js)

                       <Link to='/home/#section1'></Link>
                        and other page put a id name

                         id={'section1'} */}
                    <div className={`${showLinks ? 'links-container show-container' : 'links-container'}`}>
                       <ul className="links my-auto">
                          <li className="mr-3 linksNav"
                                         onClick={scrollToSectionH}                        
                          >
                             <Link to="home"
                                  // onClick={hideNav}
                             >Home</Link>
                           </li>
                           <li className="mr-3 linksNav" onClick={scrollToSectionAbout}>
                             <Link to="aboutme">About</Link>
                           </li>
                           <li className="mr-3 linksNav" onClick={scrollToSectionP}>
                             <Link to="projects">Projects</Link>
                           </li>
                           <li className="mr-3 linksNav" onClick={scrollToSectionS}>
                             <Link to="myskills">My Skills</Link>
                           </li>
 
                       {/* <Link to="/home" className="btn btn-dark">Home</Link>
                       <Link to="/aboutme" className="btn btn-dark">About Me</Link>
                       <Link to="/projects" className="btn btn-dark" activeClassName="active">Projects</Link>
                       <Link to="/myskills" className="btn btn-dark" activeClassName="active">My Skills</Link>  */}
                
                         {/* {
                             links.map(link => {
                                 const { id, url, text } = link;
                                 return (
                                     <li key={id} className="mr-4" onClick={scrollToSection}>
                                         <div className={ navbar ? 'activosLinks' : ''}>
                                             {text}
                                         </div>
                                     </li>
                                 )
                             })
                         } */}
                       </ul>
                    </div>
                 {/* } */}
                    
                    <ul className="social-icons">
                      {
                          social.map(item => {
                              const { id, url, icon } = item;
                              return (
                                  <li key={id}>
                                      <a className="socialIcons" href={url} className={ navbar ? 'activosSocial' : ''}>
                                          {icon}
                                      </a>
                                  </li>
                              )
                          })
                      }
                    </ul>
                </div>
        </nav>
        </>
    )
}

export default Navbar


