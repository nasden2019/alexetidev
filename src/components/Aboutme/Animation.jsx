import React, { useState } from 'react';
import info from './Languages';
// import { motion, animatePresence } from 'framer-motion'
// import { FaArrowRight} from 'react-icons/fa';
// import './App.css';
// import Aos from 'aos'


const Animation = () => {

  const [jobs, setJobs] = useState(info);
  const [value, setValue] = useState(0);

  // const [showAnimation, setAnimation] = React.useState();

  const { flag, id, text } = jobs[value];
  return (
  // <motion.div
  // animate={{
  //   scale: [1, 2, 2, 1, 1],
  //   rotate: [0, 10, 270, 270, 0],
  //   borderRadius: ["20%", "20%", "150%", "50%", "120%"],
  // }}
  //         //  animate={{ rotate: 360 }}
  //         //  transition={{ ease: "easeOut", duration: 3 }}
  //        >
    <section className="sectionAbout" id="aboutme">
    
    <div className="title" >
      <h2 className="h2title">About Me</h2>
      <div className="titleUnderline"></div>
    </div>

    <div className="jobs-center">

      <div className='btn-container mt-5'>
        {jobs.map((item, index) => {
          return (
            <img
              src ={item.flag}
              key={item.id}
              onClick={() => setValue(index)}
              className={`job-btn mb-5 ${index === value && 'active-btn'}`}
             />
          )
        })}
      </div>

      <article className="job-info mt-4">
        {text.map((line, index) => {
          return <div key={index} className="job-desc">
            {/* <FaArrowRight /> */}
        <p className="textAbout">{line}</p>
          </div>
        })}
      </article>
    </div>

  </section>
  // </motion.div>
  )
}

export default Animation;
